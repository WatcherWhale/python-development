CTOK = 273.15

cel = float(input("Temperature (°C)? "))

kel = cel + CTOK
fah = cel * 1.8 + 32

print("%.2f K" % kel)
print("%.2f °F" % fah)
