PSMALL = 0.12
PLARGE = 0.25

aSmall = input("Amount of small bottles? ")
aLarge = input("Amount of large bottles? ")

price = round(float(aSmall) * PSMALL + float(aLarge) * PLARGE, 2)

print("%0.2f" % price)
