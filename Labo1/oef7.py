amount = float(input("Amount in bank account? "))

INTEREST = 1 + 1.2 / 100

y1 = amount * INTEREST
y2 = y1     * INTEREST
y3 = y2     * INTEREST

print("Year 1 %0.2f" % y1)
print("Year 2 %0.2f" % y2)
print("Year 3 %0.2f" % y3)
