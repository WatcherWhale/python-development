import math

r = float(input("r? "))
h = float(input("h? "))

volume = r ** 2 * math.pi * h
volume = round(volume, 1)

print("Volume %0.1f" % volume)
