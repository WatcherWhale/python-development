n = 0
total = [0, 0, 0, 0]

while True:
    agestr = input("Age or blank? ")

    if agestr == "":
        break

    age = int(agestr)

    n += 1

    if age < 3:
        total[0] +=1
    elif age < 12:
        total[1] += 1
    elif age < 65:
        total[2] += 1
    else:
        total[3] += 1

total[0] = 0
total[1] *= 15
total[2] *= 30
total[3] *= 20

totalPrice = total[1] + total[2] + total[3]

print(".2f" % totalPrice)
print("[< 3, 3 - 12, 13 - 64, 65+]")
print(total)
print(n)
