lower_start = 65
upper_start = 97

shift = int(input("Shift? "))
text = input("Text? ")

encrypted = ""

for c in text:
    charNum = ord(c)

    encNum = charNum - shift

    if charNum < upper_start:
        encNum = ((encNum - lower_start) % 26) + lower_start
    else:
        encNum = ((encNum - upper_start) % 26) + upper_start

    encrypted += chr(encNum)

print(encrypted)


