import math

word = input("Word? ")

toCheck = math.floor(len(word) / 2)


palindrome = True
for i in range(toCheck):
    palindrome = palindrome and word[i] == word[len(word) - 1 - i]

if palindrome:
    print("Palindrome")
else:
    print("Boring word")
