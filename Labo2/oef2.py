years = int(input("Dog age in years? "))

if years < 0:
    print("Your dog has not yet been born!")
    exit(1)

if years <= 2:
    dogyears = years * (10.5 / 2)
    print("Your dog is %.2f dog years old" % dogyears)
    exit(0)


dogyears = 10.5 + (years - 2) * 4
print("Your dog is %.2f dog years old" % dogyears)
