maanden = {
    'januari': '31',
    'februari': '28 / 29',
    'maart': '31',
    'april': '30',
    'mei': '31',
    'juni': '30',
    'juli': '31',
    'augustus': '31',
    'september': '30',
    'oktober': '31',
    'november': '30',
    'december': '31',
}

dagen = maanden[input("Maand? ")]
print("%s dagen" % dagen)
