months = [
    'januari'
    'februari'
    'maart'
    'april'
    'mei'
    'juni'
    'juli'
    'augustus'
    'september'
    'oktober'
    'november'
    'december']

month = months.index(input("Maand? ")) + 1
day = int(input("Dag? "))

if month in [1, 2] or ( month == 3 and day < 20):
    print("Winter")
elif month in [4, 5] or ( month == 6 and day < 21 ) or month == 3:
    print("Lente")
elif month in [7, 8] or ( month == 9 and day < 22 ) or month == 6:
    print("Zomer")
else:
    print("Herfst")

