year = int(input("Year? "))

if year % 400 == 0 or (year % 4 == 0 and not year % 100 == 0):
    print("Schrikkeljaar")
else:
    print("Een gewoon jaar")
