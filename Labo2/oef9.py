import random

roll = random.randint(0, 37)

if roll == 36:
    print("Pay 0")
    exit(0)
elif if roll == 37:
    print("Pay 00")
    exit(0)

print("Balletje beland op %d" % (roll + 1))
print("betaal %d" % (roll + 1))

if (roll + 1) % 2 == 0:
    print("betaal oneven")
else:
    print("betaal even")

if ( roll + 1 ) in [1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36]:
    print("betaal rood")
else:
    print("betaal zwart")

if roll < 18:
    print("Betaal 1 tem 18")
else:
    print("Betaal 19 tem 36")
