import math

def main():
    print(pythagoras(3, 5))


def pythagoras(a, b):
    return math.sqrt(a ** 2 + b ** 2)


if __name__ == "__main__":
    main()
