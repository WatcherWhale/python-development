import random

def main():
    print(generatePassword())
    print(generatePassword())
    print(generatePassword())
    print(generatePassword())

def generatePassword():
    l = random.randint(7, 10)
    password = ""

    for i in range(l):
        password += chr(random.randint(33, 126))
    return password


if __name__ == "__main__":
    main()
