import re

def main():
    print(checkPassword("abcdefghijk"))
    print(checkPassword("Abcdefghijk"))
    print(checkPassword("Abcdefghijk123"))
    print(checkPassword("Ab123"))

def checkPassword(password):
    if len(password) < 8:
        return False

    nums = len(re.findall("[0-9]", password))
    uppers = len(re.findall("[A-Z]", password))
    lowers = len(re.findall("[a-z]", password))

    return nums > 0 and uppers > 0 and lowers > 0

if __name__ == "__main__":
    main()
