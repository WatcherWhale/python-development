from oef10 import generatePassword
from oef11 import checkPassword

def main():
    password = generatePassword()
    good = checkPassword(password)

    print(password)
    print(good)

if __name__ == "__main__":
    main()
