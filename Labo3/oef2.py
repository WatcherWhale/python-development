def main():
    print(taxi(2))
    print(taxi(20))
    print(taxi(30))
    print(taxi(55, airport=True))
    print(taxi(30, weekend=True))


def taxi(distance, weekend = False, airport = False):
    if airport:
        return 78 + 2 * (distance - 50)

    if distance <= 5:
        return distance * 2.5
    if distance <= 20:
        mult = 2.2
        if weekend:
            mult += 0.1
        return distance * mult
    else:
        mult = 2
        if weekend:
            mult += 0.1
        return distance * mult



if __name__ == "__main__":
    main()
