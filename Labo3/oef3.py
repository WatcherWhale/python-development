def main():
    print(tarif(0))
    print(tarif(1))
    print(tarif(2))
    print(tarif(10))

def tarif(amount):
    if amount == 0:
        return 0

    return 8 + (amount - 1) * 3

if __name__ == "__main__":
    main()
