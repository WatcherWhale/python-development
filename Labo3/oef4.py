def main():
    print(median(1,2,3))
    print(median(5,6,6))
    print(median(9, 6, 7))

def median(*nums):

    nums = list(nums)
    nums.sort()

    if len(nums) % 2 == 0:
        return
    else:
        index = (len(nums) - 1) / 2
        return nums[int(index)]

if __name__ == "__main__":
    main()
