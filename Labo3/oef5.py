def main():
    print(spacing("Hello", 7))
    print(spacing("World", 7))
    print(spacing("and", 7))
    print(spacing("other", 7))
    print(spacing("humanbeings", 7))

def spacing(string, length):
    for i in range( int((length - len(string)) / 2) ):
        string = " " + string
    return string

if __name__ == "__main__":
    main()
