def main():
    print(captalize("hEyHo"))
    print(captalize("    hey"))
    print(captalize("lol LOL"))

def captalize(string):
    string = string.lower()

    firstLetter = len(string) - len(string.lstrip())

    if firstLetter == 0:
        string = string[0].upper() + string[1:]
    else:
        string = string[:firstLetter - 1] + string[firstLetter].upper() + string[firstLetter + 1:]

    return string

if __name__ == "__main__":
    main()
