import re

def main():
    print(is_int("  8"))
    print(is_int("  +8"))
    print(is_int("  -8"))
    print(is_int("  test1"))
    print(is_int("  test"))

def is_int(string):
    string = string.strip()

    # Q: How to make something simple overly complex
    # A: Like this
    if re.match("^\+?\-?[0-9]*$", string):
        return True
    else:
        return False

if __name__ == "__main__":
    main()
