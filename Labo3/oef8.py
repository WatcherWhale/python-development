import math

def main():
    print(is_prime(0));
    print(is_prime(1));
    print(is_prime(4));
    print(is_prime(10));
    print(is_prime(7));
    print(is_prime(11));
    print(is_prime(16));

def is_prime(num):
    if num <= 1:
        return False

    for i in range(2, int(math.ceil(math.sqrt(num))) + 1):
        if i == num:
            continue

        if num % i == 0:
            return False
    return True

if __name__ == "__main__":
    main()
