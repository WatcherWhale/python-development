from oef8 import is_prime

def main():
    print(nextPrime(1));
    print(nextPrime(5));
    print(nextPrime(100));

def nextPrime(num):
    next = num + 1
    while not is_prime(next):
        next += 1

    return next


if __name__ == "__main__":
    main()
