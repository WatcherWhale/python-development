#!/usr/bin/env python3

import random

def main():
    bCard = gen_bingo()
    display_bingo(bCard)

def gen_bingo() -> dict:
    card = {}

    card["B"] = [random.randint(1, 15) for i in range(5)]
    card["I"] = [random.randint(1, 15) + 15 for i in range(5)]
    card["N"] = [random.randint(1, 15) + 30 for i in range(5)]
    card["G"] = [random.randint(1, 15) + 45 for i in range(5)]
    card["O"] = [random.randint(1, 15) + 60 for i in range(5)]

    return card

def display_bingo(card: dict):
    print(" B   I   N   G   O")
    print(":##::##::##::##::##:")

    for i in range(5):
        line  = " %02d " % card["B"][i]
        line += " %02d " % card["I"][i]
        line += " %02d " % card["N"][i]
        line += " %02d " % card["G"][i]
        line += " %02d " % card["O"][i]
        print(line)


if __name__ == "__main__":
    main()
