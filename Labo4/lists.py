#!/usr/bin/python

def main():

    print(is_sublist([1,2,3,4,5,6,7,8,9], [2,3,4]))
    print(is_sublist([1,2,3,4,5,6,7,8,9], [4,5,6]))
    print(is_sublist([1,2,3,4,5,6,7,8,9], [7, 2, 3]))
    print(gen_sublists([1,2,3,4,5,6,7,8,9]))
    pass

def is_sublist(bigList: list, smallList: list) -> bool:
    if len(smallList) > len(bigList):
        return False

    if len(smallList) == 0:
        return True

    pos = 0
    for i in smallList:
        try:
            pos = bigList.index(i, pos)
        except ValueError:
            return False

    return True

def gen_sublists(bigList: list) -> list:
    lists = []

    #if len(bigList) == 0:
    #    return lists

    for l in range(len(bigList) + 1):
        for i in range(len(bigList) - l + 1):
            subList = bigList[i:i+l]

            if subList not in lists:
                lists.append(subList)

    #lists.append(bigList)

    return lists


if __name__ == "__main__":
    main()
