#!/usr/bin/env python3

def main():
    inp = input("Word? ").upper()
    print(calc(inp))


def calc(word: str) -> int:
    score = 0
    for ch in word:
        score += vals[ch]
    return score


vals = {
    "A": 1,
    "B": 3,
    "C": 5,
    "D": 2,
    "E": 1,
    "F": 4,
    "G": 3,
    "H": 4,
    "I": 1,
    "J": 4,
    "K": 3,
    "L": 3,
    "M": 3,
    "N": 1,
    "O": 1,
    "P": 3,
    "Q": 10,
    "R": 2,
    "S": 2,
    "T": 2,
    "U": 4,
    "V": 4,
    "W": 5,
    "X": 8,
    "Y": 8,
    "Z": 4
}

if __name__ == "__main__":
    main()
