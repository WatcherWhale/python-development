def readFile():
    pse = []
    with open("./PSE.txt", "r") as file:
        for line in file.readlines():
            line = line.strip().split(',')
            pse.append({
                "protons": line[0],
                "symbol": line[1],
                "name": line[2]
            })

    return pse

def getElementByProtons(pse, protons):
    return pse[protons - 1]

def getByNameOrSymbol(pse, search):
    for el in pse:
        if el["name"] == search or el["symbol"] == search:
            return el
    return None

def main():
    pse = readFile()
    while True:
        line = input("? ")
        try:
            num = int(line)
            print(getElementByProtons(pse, num))
        except:
            print(getByNameOrSymbol(pse, line))

if __name__ == "__main__":
    main()

