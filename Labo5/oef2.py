import sys

def main(path):
    with open(path, "r") as file:
        lines = file.readlines()
        for line in lines[-10:]:
            print(line.strip())

if __name__ == "__main__":
    main(sys.argv[1])

# Line 12
