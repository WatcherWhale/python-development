import sys

def main(paths):
    for path in paths:
        with open(path, "r") as file:
            lines = file.readlines()
            for line in lines:
                print(line.strip())

if __name__ == "__main__":
    main(sys.argv[1:])

# Line 12
