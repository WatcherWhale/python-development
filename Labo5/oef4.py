import sys

def main(path):
    with open(path, "r") as file:
        lines = file.readlines()
        for i in range(len(lines)):
            line = lines[i]
            print(f"{i + 1}: " + line.strip())

if __name__ == "__main__":
    main(sys.argv[1])

# Line 12
