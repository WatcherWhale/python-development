import sys

def main(path):
    with open(path, "r") as file:
        words = file.read().replace('\n', '').split(" ")
        words.sort(key = lambda x: len(x), reverse= True)
        print(words[0])

if __name__ == "__main__":
    main(sys.argv[1])
