import sys
import cli
import interactive

if __name__ == "__main__":

    if len(sys.argv) <= 1:
        print("Help")
        exit()

    if "-i" not in sys.argv:
        cli.cli(sys.argv[1:])
    else:
        interactive.interactive()
