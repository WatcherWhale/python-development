import json

from cli.CliExecutor import CliExecutor
from velo.veloHandler import VeloHandler

class BikeExecutor(CliExecutor):

    def __init__(self, args):
        super().__init__(args)

        self.help = """
### Bike Help ###
{{CMD}} bike --count                         : Print the amount of bikes.
{{CMD}} bike --amount                        : Print the amount of bikes.

{{CMD}} bike <id>                            : Print bike information.
{{CMD}} bike <id> maintenance [true / false] : Set the maintenance request for a bike (when no value is specified toggle).
"""

    def execute(self):
        if "--count" in self.args or "--amount" in self.args:
            print(len(VeloHandler.instance().bikes))
            return

        if len(self.args) <= 2:
            self.print({"error": "No bike id given"})
            self.exit(1)

        self.findBike(self.args[2])

        if len(self.args) == 3:
            self.printInfo()

        if "maintenance" in self.args:
            self.maintenanceToggle()

    def findBike(self, id):
        foundBike = VeloHandler.instance().getBike(int(id))

        if foundBike is None:
            self.print({"error": "Bike does not exist."})
            self.exit(1)

        self.bike = foundBike

    def printInfo(self):
        bikeInfo = vars(self.bike)

        bikeInfo["location"] = VeloHandler.instance().getBikeLocation(self.bike.id)

        self.print(bikeInfo)

    def maintenanceToggle(self):
        if len(self.args) >= 5:
            mode = not self.args[4].lower() == "false"
            VeloHandler.instance().setMaintenance(self.bike.id, mode)
        else:
            mode = not VeloHandler.instance().getMaintenance(self.bike.id)
            VeloHandler.instance().setMaintenance(self.bike.id, mode)
