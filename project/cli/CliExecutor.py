import sys
import copy

from utils.print import getPrintFunction

class CliExecutor:
    def __init__(self, args):
        self.args = copy.deepcopy(args)

        self.interactive = "-i" in self.args or "--interactive" in self.args

        self.removeArgs()

        self.print = getPrintFunction()

        self.help = ""

    def removeArgs(self):
        unused = {
            "-o": 2,
            "--output": 2,
            "--interactive": 1,
            "-i": 1
        }

        for k in unused:
            amount = unused[k]

            if k in self.args:
                index = self.args.index(k)

                for i in range(amount):
                    del self.args[index]

    def execute(self):
        raise NotImplementedError("The execute method has not been implemented")

    def printHelp(self):
        print(self.help.replace("{{CMD}}", self.args[0]).strip())

    def exit(self, code=0):
        if not self.interactive:
            sys.exit(code)


