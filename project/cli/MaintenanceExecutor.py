import copy
import json

from cli.CliExecutor import CliExecutor
from velo.veloHandler import VeloHandler

class MaintenanceExecutor(CliExecutor):

    def __init__(self, args):
        super().__init__(args)

        self.help = """
### Maintenance Help ###
{{CMD}} maintenance relocate <station> <slot>       : Relocate bike in a specific slot from a station.
{{CMD}} maintenance replace <bike> <station> <slot> : Replace bike to a slot from a station.
"""

    def execute(self):
        try:
            if self.args[2] == "relocate":
                VeloHandler.instance().relocateBikeSlot(int(self.args[3]), int(self.args[4]))
            elif self.args[2] == "replace":
                VeloHandler.instance().relocateBikeSlot(int(self.args[3]), int(self.args[4]), int(self.args[5]))
        except Exception as e:
            self.print({"error": e.args})
            self.exit(1)
