import json

from cli.CliExecutor import CliExecutor
from velo.veloHandler import VeloHandler

from utils.simulation import simulate

class SimulationExecutor(CliExecutor):

    def __init__(self, args):
        super().__init__(args)

        self.help = """
### Simulation Help ###
{{CMD}} simulate <steps> [offset]: Simulate an amount of simulation steps (or trips).
"""

    def execute(self):
        if len(self.args) == 3:
            simulate(int(self.args[2]))
        elif len(self.args) == 4:
            simulate(int(self.args[2]), int(self.args[3]))
        else:
            self.print({
                "error": "Invalid arguments."
            })
            self.exit(1)

