import copy
import json

from cli.CliExecutor import CliExecutor
from velo.veloHandler import VeloHandler

class StationExecutor(CliExecutor):

    def __init__(self, args):
        super().__init__(args)

        self.help = """
### Station Help ###
{{CMD}} station --count            : Print the amount of stations.
{{CMD}} station --amount           : Print the amount of stations.

{{CMD}} station <id>               : Print station information.
{{CMD}} station <id> --slots       : Print all station slots.
{{CMD}} station <id> --all-slots   : Print all station slots.
{{CMD}} station <id> --full-slots  : Print all full station slots.
{{CMD}} station <id> --empty-slots : Print all empty station slots.
"""

    def execute(self):
        if "--count" in self.args or "--amount" in self.args:
            print(len(VeloHandler.instance().stations))
            return

        if len(self.args) <= 2:
            self.print({"error": "No station id given"})
            self.exit(1)

        self.findStation(self.args[2])

        if len(self.args) == 3:
            self.printInfo()

        if "--all-slots" in self.args or "--slots" in self.args:
            self.printSlots()

        if "--full-slots" in self.args:
            self.printSlots(True)

        if "--empty-slots" in self.args:
            self.printSlots(False)

    def findStation(self, id):
        foundStation = copy.deepcopy(VeloHandler.instance().getStation(int(id)))

        if foundStation is None:
            self.print({"error": "Station does not exist"})
            self.exit(1)

        self.station = foundStation

    def printInfo(self):
        amount = len(self.station.slots)
        self.station.slots = amount
        self.print(self.station)

    def printSlots(self, full = None):

        if full == True:
            slots = self.station.getFullSlots()
        elif full == False:
            slots = self.station.getEmptySlots()
        else:
            slots = self.station.slots

        slots = list(map(lambda x: {"id": x.id, "bike": x.bike}, slots))

        if full == False:
            slots = list(map(lambda x: x["id"], slots))

        self.print(slots)
