from cli.CliExecutor import CliExecutor
from velo.veloHandler import VeloHandler

class TripExecutor(CliExecutor):

    def __init__(self, args):
        super().__init__(args)

        self.help = """
### Trip Help ###
{{CMD}} trip start <user id> <station id>       : Start a trip for a specific user.
{{CMD}} trip stop <user id> <station id> <slot> : Finish a trip for a specific user.
{{CMD}} trip end <user id> <station id> <slot>  : Finish a trip for a specific user.
"""

    def execute(self):
        veloHandler = VeloHandler.instance()

        if self.args[2] == "start":
            try:
                self.print(veloHandler.startTrip(int(self.args[3]), int(self.args[4])))
            except ValueError as e:
                self.print({
                    "error": e.args[0],
                })
                self.exit(1)
        elif self.args[2] == "stop" or self.args[2] == "end":
            try:
                self.print(veloHandler.finishTrip(int(self.args[3]), int(self.args[4]), int(self.args[5])))
            except ValueError as e:
                self.print({
                    "error": e.args[0],
                })
                self.exit(1)
