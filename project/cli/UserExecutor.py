import copy
import json

from cli.CliExecutor import CliExecutor
from velo.veloHandler import VeloHandler

class UserExecutor(CliExecutor):

    def __init__(self, args):
        super().__init__(args)

        self.help = """
### User Help ###
{{CMD}} user --count       : Print the amount of users.
{{CMD}} user --amount      : Print the amount of users.

{{CMD}} user <id>         : Print user information.
{{CMD}} user <id> --trips : Print all trips for a specific user.
"""


    def execute(self):
        if "--count" in self.args or "--amount" in self.args:
            print(len(VeloHandler.instance().users))
            return

        if len(self.args) <= 2:
            self.print({"error": "No user id given"})
            self.exit(1)

        self.findUser(self.args[2])

        if len(self.args) == 3:
            self.printInfo()

        if "--trips" in self.args:
            self.getTrips()

    def findUser(self, id):
        foundUsers = filter(lambda x: x.id == int(id), VeloHandler.instance().users)
        foundUser = copy.deepcopy(next(foundUsers, None))

        if foundUser is None:
            self.print({"error": "User does not exist"})
            self.exit(1)

        self.user = foundUser

    def printInfo(self):
        self.print(self.user)

    def getTrips(self):
        trips = VeloHandler.instance().getUserTrips(self.user.id)

        self.print(list(trips))


