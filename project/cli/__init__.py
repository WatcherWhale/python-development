from cli.TripExecutor import TripExecutor
from cli.UserExecutor import UserExecutor
from cli.StationExecutor import StationExecutor
from cli.BikeExecutor import BikeExecutor
from cli.MaintenanceExecutor import MaintenanceExecutor
from cli.SimulationExecutor import SimulationExecutor

cliExcutors = {
    "trip": TripExecutor,
    "user": UserExecutor,
    "station": StationExecutor,
    "bike": BikeExecutor,
    "maintenance": MaintenanceExecutor,
    "simulate": SimulationExecutor
}
