import json
import pickle
import random

from velo.station import Station
from velo.user import User
from velo.bike import Bike

def generateData():
    with open("extras/locations.json") as file:
        locs = json.load(file)

    stations = []

    for loc in locs['features']:
        props = loc['properties']

        street = props['Straatnaam'].replace(" (" + str(props['Postcode']) + ")", "")

        station = Station(loc['id'], street + " (" + str(loc['id']) + ")", props['Aantal_plaatsen'], \
                          street + " " + props['District'] + " " + props['Postcode'], \
                          tuple(loc['geometry']['coordinates']))

        stations.append(station)

    with open("extras/firstnames.txt") as file:
        firstnames = file.readlines()
    with open("extras/lastnames.txt") as file:
        lastnames = file.readlines()

    users = []
    for id in range(5000):
        users.append(User(id, random.choice(firstnames).rstrip(), random.choice(lastnames).rstrip()))

    bikes = []
    for id in range(4000):
        bikes.append(Bike(id))


    for bike in bikes:
        isFull = True
        while isFull:
            station = random.choice(stations)
            slots = station.getEmptySlots()

            if len(slots) == 0:
                continue

            isFull = False

            slot = random.choice(slots)
            station.addBike(bike.id, slot.id)

    return (stations, users, bikes )
