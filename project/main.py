#!/usr/bin/env python3

import sys
import json
import pickle

from velo.veloHandler import VeloHandler

from velo.bike import Bike
from velo.user import User
from velo.station import Station

from cli import cliExcutors

from utils.templater import generateSite

def main(args):
    stations, users, bikes = [], [], []

    if "--generate" in args:
        VeloHandler.instance().generate()
        sys.exit()
    else:
        try:
            VeloHandler.instance().load()
        except FileNotFoundError:
            VeloHandler.instance().generate()
        except:
            print("Something went wrong")
            sys.exit(1)

    if "--site" in args:
        site()
        sys.exit()

    if "-i" in args or "--interactive" in args:
        interactive()
    else:
        cli(args)
        save()

def interactive():
    while True:
        inp = input("> ")

        if inp.lower() == "save":
            save()
            continue
        if inp.lower() == "exit":
            return

        args = inp.split(" ")
        args.insert(0, "")

        cli(args, True)

def cli(args, interactive = False):
    # Print help when not a valid command
    if len(args) <= 1 or args[1] not in cliExcutors:
        print("""
### General Help ###
{{CMD}} --help     : This list.
{{CMD}} -h         : This list.
{{CMD}} --generate : Generate a new start situation.
{{CMD}} --site     : Create a static HTML site.

### Interactive Shell ###
{{CMD}} -i            : Start a interactive velo shell.
{{CMD}} --interactive : Start a interactive velo shell.

save : Save interactive / simulation.
exit : Exit interactive / simulation.

### Extra options ###
--output <json / yaml> : Specify the output format.
-o       <json / yaml> : Specify the output format.
              """.replace("{{CMD}}", args[0]).lstrip())

        for e in cliExcutors:
            executor = cliExcutors[e](args)
            executor.printHelp()
            print()

        # Print error code when no help page is requested
        if not interactive:
            sys.exit("--help" not in args and "-h" not in args)
        else:
            return

    # Get the correct cli executor
    executor = cliExcutors[args[1]](args)

    # Print help page if requested
    if "--help" in args or "-h" in args:
        executor.printHelp()
        if not interactive:
            sys.exit()
        else:
            return

    # Execute cli context
    executor.execute()

def site():
    generateSite()

def save():
    VeloHandler.instance().save()

if __name__ == "__main__":
    main(sys.argv)
