# Velo Antwerpen
Mathias Maes

## CLI

Ik in plaat van een interactieve applicatie een CLI tool gemaakt. Dit maakt het
handig om mijn applicatie te automatiseren via bash scripts, of periodiek aan
te roepen (De statische website periodiek updaten).

Je kan alles uitvoeren op een UNIX aparaat met `./main.py` (= executable file)
of op Windows met `python main.py`.

#### Help Pagina's

Omdat ik gekozen heb voor een CLI tool, heb ik ook voor alle commando's een
help pagina gemaakt. Je kan de standaar help pagina opvragen door 
`./main.py -h` of `./main.py --help` uit te voeren.

Om meer specifieke help pagina's te krijgen kan je `./main.py <command> -h` of
`./main.py <command> --help` uitvoeren.

**Standaard help pagina**
```console
$ ./main.py --help
### General Help ###
./main.py --help     : This list.
./main.py -h         : This list.
./main.py --generate : Generate a new start situation.
./main.py --site     : Create a static HTML site.

### Interactive Shell ###
./main.py -i            : Start a interactive velo shell.
./main.py --interactive : Start a interactive velo shell.

save : Save interactive / simulation.
exit : Exit interactive / simulation.

### Extra options ###
--output <json / yaml> : Specify the output format.
-o       <json / yaml> : Specify the output format.

### Trip Help ###
./main.py trip start <user id> <station id>       : Start a trip for a specific user.
./main.py trip stop <user id> <station id> <slot> : Finish a trip for a specific user.
./main.py trip end <user id> <station id> <slot>  : Finish a trip for a specific user.

### User Help ###
./main.py user --count       : Print the amount of users.
./main.py user --amount      : Print the amount of users.

./main.py user <id>         : Print user information.
./main.py user <id> --trips : Print all trips for a specific user.

### Station Help ###
./main.py station --count            : Print the amount of stations.
./main.py station --amount           : Print the amount of stations.

./main.py station <id>               : Print station information.
./main.py station <id> --slots       : Print all station slots.
./main.py station <id> --all-slots   : Print all station slots.
./main.py station <id> --full-slots  : Print all full station slots.
./main.py station <id> --empty-slots : Print all empty station slots.

### Bike Help ###
./main.py bike --count                         : Print the amount of bikes.
./main.py bike --amount                        : Print the amount of bikes.

./main.py bike <id>                            : Print bike information.
./main.py bike <id> maintenance [true / false] : Set the maintenance request for a bike (when no value is specified toggle).

### Maintenance Help ###
./main.py maintenance relocate <station> <slot>       : Relocate bike in a specific slot from a station.
./main.py maintenance replace <bike> <station> <slot> : Replace bike to a slot from a station.

### Simulation Help ###
./main.py simulate <steps> [offset]: Simulate an amount of simulation steps (or trips).
```

### Interactive Custom shell

Als je graag een bulk van CLI commando's wil uitvoeren zonder na elk commando
alles te saven (wat voor vertraging zorgt), kan je gebruiken maken van de
ingebouwde shell. Hier voor je dezelfde commando's uit als op de CLI, maar dan
zonder de executable ervoor. Voor het saven gebruik je het commando `save`. Als
je deze modus wil verlateen zal het `exit` commando je helpen.

### Output format

Omdat ik de Velo tool met extenrne automatisatie in mind heb gemaakt, zal je
ook machine readable output krijgen op stdout. Deze is beschikbaar in
2 formaten `yaml` en `json`. Standaard is dit in yaml, omdat dit ook meer
leesbaar is voor mensen, dan json. Je kan met het output
formaat dat je wil specifieren met `-o <format>`.

**Voorbeelden**

```console
$ ./main.py user 10 --trips -o yaml
- bike: 3317
  destination:
    address: Boudewijnssteeg ANTWERPEN 2018
    coordiniates:
    - 4.39932133135339
    - 51.20553899549738
    id: 229134
    name: Boudewijnssteeg (229134)
  start:
    address: Ringlaan MERKSEM 2170
    coordiniates:
    - 4.45439058485794
    - 51.254540303068104
    id: 229142
    name: Ringlaan (229142)
  startTime: 15872
  stopTime: 15873
  user: 10

$ ./main.py user 10 --trips -o json | jq
[
  {
    "user": 10,
    "bike": 3317,
    "start": {
      "id": 229142,
      "name": "Ringlaan (229142)",
      "address": "Ringlaan MERKSEM 2170",
      "coordiniates": [
        4.45439058485794,
        51.254540303068104
      ]
    },
    "destination": {
      "id": 229134,
      "name": "Boudewijnssteeg (229134)",
      "address": "Boudewijnssteeg ANTWERPEN 2018",
      "coordiniates": [
        4.39932133135339,
        51.20553899549738
      ]
    },
    "startTime": 15872,
    "stopTime": 15873
  }
]
```

## Simulatie

Om de simuatie op te starten gebruik je `./main.py simulate <steps>` met het
`steps` argument kan je de hoeveelheid simulatie stappen of trips die worden
gesimuleerd specifieren.

Je zal ook zien dat hier een progressbar te voorschijn komt, dit komt omdat
simulatie soms zeer lang kan duren en hiermee kan je zien hoe lang het nog
duurt hoeveel stappen er nog moeten worden uitgevoerd etc.

## Site generation

De website wordt gemaakt met [mustache](https://mustache.github.io/) een
templating engine beschikbaar in een grote hoeveelheid talen.

Ik maak ook gebruik van [bulma](https://bulma.io), dit is een css framework waardor het veel makkelijker wordt om mooie html pagina's te maken.

## Moeilijkheden

Ik heb af en toe moeilijkheden tegengekomen, deze waren voornamelijk een
probleem met references. Voor het displayen laat ik niet altijd alles zien en
deze velden verwijderde ik dan, echter savede het project deze ook en kreeg ik
bij de volgende execution vaak errors. Dit was opgelost met een deepocopy.
