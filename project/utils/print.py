import json
import yaml
import sys

def getPrintFunction():
    if "-o" in sys.argv:
        i = sys.argv.index("-o")
    elif "--output" in sys.argv:
        i = sys.argv.index("--output")
    else:
        return printYaml

    if sys.argv[i + 1] == "json":
        return printJson
    elif sys.argv[i + 1] == "yaml" or sys.argv[i + 1] == "yml":
        return printYaml
    else:
        raise ValueError("Invalid output format.")

def obj2serializable(obj) -> dict:
    if isinstance(obj, str) or isinstance(obj, int)\
            or isinstance(obj, float) or isinstance(obj, bool)\
            or obj is None:
        return obj
    elif isinstance(obj, dict):
        for key in obj:
            obj[key] = obj2serializable(obj[key])
        return obj
    elif isinstance(obj, list) or isinstance(obj, tuple):
        return list(map(lambda x: obj2serializable(x), obj))
    else:
        return vars(obj)

def printJson(obj):
    print(json.dumps(obj2serializable(obj)))

def printYaml(obj):
    yaml.SafeDumper.ignore_aliases = lambda *args: True
    print(yaml.safe_dump(obj2serializable(obj)).rstrip())
