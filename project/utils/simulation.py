import random

from velo.veloHandler import VeloHandler

from tqdm import tqdm


def simulate(steps = 1000, offset = 0):
    handler = VeloHandler.instance()

    users = handler.getUserIds()
    stations = handler.getStationIds()

    for it in tqdm(range(steps), unit=" steps"):

        user = random.choice(users)
        start = random.choice(stations)
        stop = random.choice(stations)

        while not startTrip(user, start, it + offset):
            user = random.choice(users)
            start = random.choice(stations)

        while not stopTrip(user, stop, it + offset):
            stop = random.choice(stations)

def startTrip(user, start, it):
    handler = VeloHandler.instance()
    try:
        handler.startTrip(user, start, it * 2)
        return True
    except:
        return False

def stopTrip(user, stop, it):
    handler = VeloHandler.instance()
    try:
        station = handler.getStation(stop)
        slots = station.getEmptySlots()

        if len(slots) == 0:
            return False

        slot = random.choice(slots)

        handler.finishTrip(user, stop, slot.id, it * 2 + 1)

        return True
    except:
        return False
