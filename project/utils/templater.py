import os
import sys
import shutil

import chevron
from tqdm import tqdm

from velo.veloHandler import VeloHandler

def getTemplate(template: str) -> str:
    with open("templates/" + template + ".html") as file:
        return "\n".join(file.readlines())

def generateSite():

    try:
        shutil.rmtree("_site")
    except:
        pass

    try:
        os.mkdir("_site")
        os.mkdir("_site/stations")
        os.mkdir("_site/users")
        os.mkdir("_site/bikes")
    except:
        print("Failed to create folders.")
        sys.exit(1)

    handler = VeloHandler.instance()

    stationHtml = getTemplate("station")
    userHtml = getTemplate("user")
    bikeHtml = getTemplate("bike")

    print("Generating stations html...")
    for station in tqdm(handler.getStationIds(), unit=" files"):
        with open("_site/stations/" + str(station) + ".html", "w") as file:
            file.writelines(chevron.render(stationHtml, handler.getStationTemplate(station)).split("\n"))

    print("Generating user html...")
    for user in tqdm(handler.getUserIds(), unit=" files"):
        with open("_site/users/" + str(user) + ".html", "w") as file:
            file.writelines(chevron.render(userHtml, handler.getUserTemplate(user)).split("\n"))

    print("Generating bike html...")
    for bike in tqdm(handler.getBikeIds(), unit=" files"):
        with open("_site/bikes/" + str(bike) + ".html", "w") as file:
            file.writelines(chevron.render(bikeHtml, handler.getBikeTemplate(bike)).split("\n"))




