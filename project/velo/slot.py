class Slot:
    def __init__(self, id, station):
        self.id = id
        self.station = station

        self.bike = None

    def isEmpty(self):
        return self.bike is None
