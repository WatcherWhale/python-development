import random

from .bike import Bike
from .slot import Slot

class Station:
    def __init__(self, id, name, slots, address, coords):
        self.id = id
        self.name = name
        self.address = address
        self.coordiniates = coords

        self.slots = [ Slot(i, self.id) for i in range(1, slots + 1) ]

    def getEmptySlots(self):
        return list(filter(lambda x: x.bike is None, self.slots))

    def getFullSlots(self):
        return list(filter(lambda x: x.bike is not None, self.slots))

    def getAvailableSlots(self, inMaintenance = []):
        return list(filter(lambda x: x.bike is not None and x.bike not in inMaintenance, self.slots))

    def getBike(self, inMaintenance = []):
        fullSlots = self.getAvailableSlots(inMaintenance=inMaintenance)

        if len(fullSlots) == 0:
            raise ValueError("No bikes to take, station is empty.")

        slot = random.choice(fullSlots)

        return self.removeBike(slot.id)

    def returnBike(self, bike, slot):
        if self.slots[slot - 1].bike is not None:
            raise ValueError("Placed a bike on a non empty slot.")

        self.addBike(bike, slot)

    def removeBike(self, slot):

        if self.slots[slot - 1].bike is None:
            raise ValueError("Cannot take bike from empty slot.")

        bike = self.slots[slot - 1].bike
        self.slots[slot - 1].bike = None
        return bike

    def addBike(self, bike, slot):
        self.slots[slot - 1].bike = bike
