import pickle

class Trip:
    def __init__(self, user, bike, start, datetime):
        self.user = user
        self.bike = bike
        self.start = start
        self.destination = None
        self.startTime = datetime
        self.stopTime = None

    def finish(self, destination, datetime):
        self.destination = destination
        self.stopTime = datetime
