import time
import copy
import pickle

from utils import Singleton
from utils.print import obj2serializable

from velo.bike import Bike
from velo.user import User
from velo.trip import Trip
from velo.station import Station

from dataGenerator import generateData

@Singleton
class VeloHandler:
    def __init__(self):
        self.users = []
        self.stations = []
        self.bikes = []
        self.trips = []

        self.filePath = "save.bin"

    def startTrip(self, user, start, date = -1):
        """
        Start a trip for a user.
        """
        trip = next(filter(lambda x: x.user == user and x.destination is None, self.trips), None)

        if trip is not None:
            raise ValueError("User already has a bike.")

        if date == -1:
            date = int(time.time())

        startStation = self.getStationIndex(start)
        if startStation is None:
            raise ValueError("Invalid station id.")


        bike = self.stations[startStation].getBike(self.getBikesInMaintenance())

        trip = Trip(user, bike, start, date)
        self.trips.append(trip)

        return trip

    def finishTrip(self, user, destination, slot, date = -1):
        """
        Finish a already started trip for a specific user.
        """
        if date == -1:
            date = int(time.time())

        destStation = self.getStationIndex(destination)
        if destStation is None:
            raise ValueError("Invalid station id.")


        trip = next(filter(lambda x: x.user == user and x.destination is None, self.trips), None)

        if trip is None:
            raise ValueError("Cannot find any unfinished trips.")

        self.stations[destStation].returnBike(trip.bike, slot)

        i = self.trips.index(trip)
        self.trips[i].finish(destination, date)

        return self.trips[i]

    def getStation(self, id) -> Station:
        """
        Get a velo station by id
        """
        return next(filter(lambda x: x.id == id, self.stations), None)

    def getStationIndex(self, id) -> Station:
        station = self.getStation(id)

        if station is None:
            return None

        return self.stations.index(station)

    def getStationIds(self):
        """
        Get all velo station ids.
        """
        return list(map(lambda x: x.id, self.stations))

    def getStationTemplate(self, id):
        """
        Get a template object for a station
        """
        station = self.getStation(id)

        return {
            "name": station.name,
            "id": id,
            "amount": len(station.getFullSlots()),
            "address": station.address,
            "full": obj2serializable(station.getFullSlots()),
            "empty": obj2serializable(station.getEmptySlots())
        }

    def getUserIds(self):
        """
        Get all user ids
        """
        return list(map(lambda x: x.id, self.users))

    def getUser(self, id) -> User:
        """
        Get a user by id
        """
        return next(filter(lambda x: x.id == id, self.users), None)

    def getUserTrips(self, user):
        """
        Get all trips of a specific user
        """
        trips = list(filter(lambda x: x.user == user and x.destination is not None, self.trips))
        trips = copy.deepcopy(trips)
        trips = map(lambda x: self.tripToObj(x), trips)

        return list(trips)

    def getUserTemplate(self, id):
        """
        Get a template object for a specific user
        """
        user = self.getUser(id)

        return {
            "id": user.id,
            "firstname": user.name,
            "lastname": user.lastname,
            "trips": self.getUserTrips(id)
        }

    def tripToObj(self, trip) -> dict:
        """
        Get a serializable/template object for a trip
        """
        tripObj = vars(trip)

        tripObj["start"] = vars(copy.deepcopy(self.getStation(trip.start)))
        tripObj["destination"] = vars(copy.deepcopy(self.getStation(trip.destination)))

        try:
            del tripObj["start"]["slots"]
            del tripObj["destination"]["slots"]
        except:
            pass

        return tripObj

    def getBike(self, bike):
        """
        Get a bike by id
        """
        bike = next(filter(lambda x: x.id == bike, self.bikes), None)

        return bike

    def getBikeIndex(self, bike):
        """
        Get the index of a bike from its id
        """
        bike = self.getBike(bike)
        return self.bikes.index(bike)

    def getBikeIds(self):
        """
        Get all bike ids
        """
        return list(map(lambda x: x.id, self.bikes))

    def getMaintenance(self, bike):
        """
        Check if a bike is in maintenance
        """
        bikeIndex = self.getBikeIndex(bike)

        if bikeIndex is None:
            raise ValueError("Invalid bike id.")

        return self.bikes[bikeIndex].maintenance

    def setMaintenance(self, bike, maintenance = True):
        """
        Toggle or set a bike in maintenance
        """
        bikeIndex = self.getBikeIndex(bike)

        if bikeIndex is None:
            raise ValueError("Invalid bike id.")

        self.bikes[bikeIndex].maintenance = maintenance

    def getBikesInMaintenance(self) -> list:
        """
        Get all bikes in maintenance
        """
        return list(map(lambda x: x.id, filter(lambda x: x.maintenance == True, self.bikes)))

    def getBikeLocation(self, bike):
        """
        Get the location(station & slot) of a bike
        """
        loc = {
            "state": "Moving",
            "station": None,
            "stationid": None,
            "slot": None
        }

        station = next(filter(lambda x: bike in list(map(lambda y: y.bike, x.slots)), self.stations), None)
        if station is not None:
            loc["state"] = "At Station"
            loc["station"] = station.name
            loc["stationid"] = station.id
            slot = next(filter(lambda x: x.bike == bike, station.slots), None)
            loc["slot"] = slot.id
        elif self.getMaintenance(bike):
            loc["state"] = "In maintenance"

        return loc

    def getBikeTemplate(self, id):
        """
        Get a bike template object
        """
        bike = vars(self.getBike(id))
        loc = self.getBikeLocation(id)

        bike["location"] = loc

        return bike

    def relocateBikeSlot(self, station, slot):
        """
        Maintenace removal of bike.
        """
        s = self.getStationIndex(station)
        bike = self.stations[s].removeBike(slot)

        return bike

    def replaceBike(self, bike, station, slot):
        s = self.getStationIndex(station)

        return self.returnBike(bike, slot)

    def generate(self):
        """
        Generate new data
        """
        self.trips = []
        self.stations, self.users, self.bikes = generateData()

        self.save()

    def load(self):
        """
        Load from a save file
        """
        with open(self.filePath, "rb") as file:
            self.users, self.stations, self.bikes, self.trips = pickle.load(file)

    def save(self):
        """
        Save current situation to a save file
        """
        with open(self.filePath, "wb") as file:
            pickle.dump( (self.users, self.stations, self.bikes, self.trips) , file)

